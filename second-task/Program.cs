﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace second_task
{
    class MyList<T> : IList<T>
    {
        private T[] list;
        private int size;
        public int Count => size;
        public bool IsReadOnly => false;

        public MyList(int count = 100)
        {
            list = new T[count];
        }
        
        public IEnumerator<T> GetEnumerator()
        {
            for (var i = 0; i < size; i++)
            {
                yield return list[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            if (size == list.Length)
                Enlarge();
            list[size] = item;
            size++;
        }

        private void Enlarge()
        {
            var temp = new T[size * 2];
            list.CopyTo(temp, 0);
            list = temp;
        }

        public void Clear()
        {
           Array.Clear(list, 0, size);
           size = 0;
        }

        public bool Contains(T item)
        {
            return size != 0 && IndexOf(item) != -1;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Array.Copy(list, 0, array, arrayIndex, size);
        }

        public bool Remove(T item)
        {
            var index = IndexOf(item);
            if (index < 0) return false;
            RemoveAt(index);
            return true;
        }
        
        public int IndexOf(T item)
        {
            return Array.IndexOf(list, item);
        }

        public void Insert(int index, T item)
        {
            if (index > size) throw new ArgumentOutOfRangeException();
            if (size == list.Length) Enlarge();
            if (index < size) Array.Copy(list, index, list, index + 1, size - index);
            list[index] = item;
            size++;
        }

        public void RemoveAt(int index)
        {
            size--;
            if (index < size) 
                Array.Copy(list, index + 1, list, index, size - index);
        }

        public T this[int index]
        {
            get
            {
                if (index < 0 || index >= size) throw new IndexOutOfRangeException();
                return list[index];
            }
            set
            {
                if (index < 0 || index >= size) throw new IndexOutOfRangeException();
                list[index] = value;
            }
        }
    }
}