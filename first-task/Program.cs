﻿using System;

namespace first_task
{
    class Program
    {
        static void Main(string[] args)
        {
            Write(ConsoleColor.Green, "Введите выражение без пробелов!");
            Write(ConsoleColor.White);
            var input = Console.ReadLine();
            if (!CheckInput(input)) return;
            Console.WriteLine(RPN.Calculate(input));
        }

        static bool CheckInput(string input)
        {
            if (input.Length == 0)
            {
                Write(ConsoleColor.Red, "Введена пустая строка!");
                return false;
            }

            var countBracket = 0;

            foreach (var currentSymbol in input)
            {
                if (currentSymbol == '(') countBracket++;
                else if (currentSymbol == ')') countBracket--;
                if (char.IsDigit(currentSymbol) || RPN.IsOperator(currentSymbol)) continue;
                Write(ConsoleColor.Red, "Не цифра и не операция! По индексу " + (input.IndexOf(currentSymbol) + 1));
                return false;
            }

            if (countBracket > 0)
            {
                Write(ConsoleColor.Red, "Не хватает закрывающей(ищ) скобки(ок)");
                return false;
            }
            if (countBracket < 0)
            {
                Write(ConsoleColor.Red, "Не хватает открывающей(ищ) скобки(ок)");
                return false;
            }

            return true;
        }

        internal static void Write(ConsoleColor color, string str = null)
        {
            Console.ForegroundColor = color;
            if (str != null) Console.WriteLine(str);
        }
    }
}