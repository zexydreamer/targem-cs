namespace first_task
{
    internal enum PriorityOperations
    {
        OpenBracket = 0,
        CloseBracket = 1,
        Minus = 2,
        Plus = 3,
        Division = 4,
        Multiplication = 5
    }
}