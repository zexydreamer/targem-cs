using System;
using System.Collections.Generic;

namespace first_task
{
    public class RPN
    {
        public static double Calculate(string input)
        {
            var output = GetExpression(input);
            return Counting(output);
        }

        private static string GetExpression(string input)
        {
            var output = "";
            var stack = new Stack<char>();
            for (var i = 0; i < input.Length; i++)
            {
                if (char.IsDigit(input[i]))
                {
                    while (!IsOperator(input[i]))
                    {
                        output += input[i];
                        i++;
                        if (i == input.Length) break;
                    }

                    output += " ";
                    i--;
                }

                if (IsOperator(input[i]))
                {
                    if (input[i] == '(')
                        stack.Push(input[i]);
                    else if (input[i] == ')')
                    {
                        var s = stack.Pop();
                        while (s != '(')
                        {
                            output += s + " ";
                            s = stack.Pop();
                        }
                    }
                    else
                    {
                        if (stack.Count > 0)
                            if (GetPriority(input[i]) <= GetPriority(stack.Peek()))
                                output += stack.Pop() + " ";
                        stack.Push(char.Parse(input[i].ToString())); 
                    }
                }
            }

            while (stack.Count > 0)
                output += stack.Pop() + " ";
            return output;
        }

        private static int GetPriority(char input)
        {
            return input switch
            {
                '(' => (int)PriorityOperations.OpenBracket,
                ')' => (int)PriorityOperations.CloseBracket,
                '+' => (int)PriorityOperations.Plus,
                '_' => (int)PriorityOperations.Minus,
                '*' => (int)PriorityOperations.Multiplication,
                '/' => (int)PriorityOperations.Division,
                _ => 6
            };
        }

        private static double Counting(string input)
        {
            var result = 0.0;
            var temp = new Stack<double>();

            for (var i = 0; i < input.Length; i++)
            {
                if (char.IsDigit(input[i])) 
                {
                    var a = "";

                    while (!char.IsSeparator(input[i]) && !IsOperator(input[i]))
                    {
                        a += input[i];
                        i++;
                        if (i == input.Length) break;
                    }
                    temp.Push(double.Parse(a));
                    i--;
                }
                else if (IsOperator(input[i]))
                {
                    double a;
                    double b;
                    try
                    {
                        a = temp.Pop();
                        b = temp.Pop();
                    }
                    catch (InvalidOperationException)
                    {
                        Program.Write(ConsoleColor.Red, "Проверьте правильно ли Вы ввели выражение!");
                        return double.NaN;
                    }

                    result = input[i] switch
                    {
                        '+' => b + a,
                        '-' => b - a,
                        '*' => b * a,
                        '/' => b / a,
                        _ => result
                    };
                    temp.Push(result);
                }
            }
            return temp.Peek();
        }

        internal static bool IsOperator(char c) =>"+-*/()".IndexOf(c) != -1;
    }
}